import Account_Java.Account;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
    
        Account account_1 = new Account("001", "Hung", 0);
        Account account_2 = new Account("002", "Tran", 1000);
    
        //credit to account
        account_1.credit(3000);
        account_2.credit(3000);

        //debit to account
        account_1.debit(1000);
        account_2.debit(5000);

        //transfer 2000 from account1 to account2
        account_1.transferTo(account_2, 2000);
        
        //transfer 2000 from account2 to account1
        account_2.transferTo(account_1, 2000);

        System.out.println("Account_1: " + account_1);
        System.out.println("Account_2: " + account_2);

    }
}
