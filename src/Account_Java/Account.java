package Account_Java;

//import java.text.DecimalFormat;

public class Account {
    
    private String id;
    private String name;
    private int balance = 0;
    
    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public String toString() {
        return "Account: [id= " + id + ", name= " + name + ", balance= " + balance +"]";
    }

    public int credit(int credit) {
        balance = balance + credit;
        return balance;
    }

    public int debit(int debit) {
       
        if (debit <= balance){
            balance = balance - debit;
            return balance;
           
        }else{
            System.out.println("Debit amount exceeded balance");
            return balance;
        }
    }

    public int transferTo(Account targeAccount, int amount) {
       
        if (amount <= balance){
            targeAccount.balance = targeAccount.balance + amount;
            this.balance = this.balance - amount;
            return targeAccount.balance;
            
        }else{
            System.out.println("The transfered amount exceeded balance");
            return balance;
        }
    }
}
